﻿namespace BBC.Greetings.Web.Model
{
    /// <summary>
    /// Greetings response model for greetings api
    /// </summary>
    public class GreetingResponse
    {
        public string Message { get; set; }
    }
}
