﻿using BBC.Greetings.Web.Model;
using System;

namespace BBC.Greetings.Web.Services
{
    public interface IGreetingService
    {
        /// <summary>
        /// Gets Greetings message for name 
        /// </summary>
        /// <param name="name">name for greeting</param>
        /// <returns></returns>
        GreetingResponse GetGreetingMessage(String name);
    }
}
