﻿using BBC.Greetings.Web.Model;

namespace BBC.Greetings.Web.Services
{
    public class GreetingService : IGreetingService
    {
        //constant string for greeting message
        public const string MESSAGE_GENERIC_STRING = "Welcome to BBC Studios ";

        public GreetingResponse GetGreetingMessage(string name)
        {
            return new GreetingResponse() { Message = MESSAGE_GENERIC_STRING + name };
        }
    }

}
