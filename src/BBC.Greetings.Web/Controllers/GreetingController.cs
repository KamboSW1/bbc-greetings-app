﻿using BBC.Greetings.Web.Model;
using BBC.Greetings.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BBC.Greetings.Web.Controllers
{
    [Route("greetings")]
    [ApiController]
    public class GreetingController : ControllerBase
    {
        readonly IGreetingService greetingService;

        /// <summary>
        /// constructor for greetings controller
        /// </summary>
        /// <param name="greetingService">greeting service instance</param>
        public GreetingController(IGreetingService greetingService)
        {
            this.greetingService = greetingService;
        }

        // GET greetings/name
        [HttpGet("{name}")]
        public ActionResult<GreetingResponse> Get(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return BadRequest();

                return Ok(greetingService.GetGreetingMessage(name));
            }
            catch
            {
                //Internal server error in case of exception
                return StatusCode(StatusCodes.Status500InternalServerError, null);
            }
        }
    }
}